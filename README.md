  <h1>Labirinto</h1>

  <h2>Que signifie "Labirinto" ?</h2>
  
  "Labirinto" signifie "labyrinthe" en espéranto.
  
  <h2>Sous quelle licence est placé le code de ce site ?</h2>
  
  Ce site a été intégralement construit à l'aide de logiciels libres. Il est lui-même placé sous la licence libre <a href="https://www.gnu.org/licenses/quick-guide-gplv3.html">GNU GPL version 3</a> ou toute version ultérieure. Si vous pensez qu'un logiciel libre est simplement un logiciel gratuit, lisez <a href="https://www.gnu.org/philosophy/free-sw.html">ceci</a>. L'ensemble du code source est disponible sur la plateforme <a href="https://framagit.org">FramaGit</a>, <a href="https://framagit.org/casatir/labirinto">ici</a>. N'hésitez pas à utiliser le système de suivi des bogues. Si vous ne connaissez pas <a href="https://framagit.org">Framagit</a> et plus généralement les outil <a href="https://framasoft.org">Framasoft</a>, vous devriez peut-être vous intéresser à leur projet "<a href="https://degooglisons-internet.org">Dégooglisons Internet</a>".
    
  <h2>Quels sont les logiciels et ressources tierces utilisées ?</h2>

  Le moteur de labyrinthe est développé en <a href="https://www.python.org">Python 3</a>, il est converti (hors ligne) en JavaScript de manière automatique avec l'outil <a href="https://www.transcrypt.org/">Transcrypt</a>. Le code Python de l'utilisateur est converti (à la volée) avec l'outil <a href="https://github.com/differentmatt/filbert">Filbert</a>. Ce site utilise le générateur de site statique <a href="https://blog.getpelican.com">Pelican</a>, c'est le système d'intégration continue des <a href="https://docs.framasoft.org/fr/gitlab/gitlab-pages.html">Gitlab Pages</a> sur <a href="https://framagit.org">FramaGit</a> qui gère sa publication automatique, à l'aide d'une image <a href="https://www.docker.com">Docker</a> disponnible <a href="https://hub.docker.com/r/casatir/pages">ici</a>.

  Les icônes utilisés sont ceux de la police <a href="https://fontawesome.com">Font Awesome</a> et l'image de titre utilise la police <a href="https://fontstruct.com/fontstructions/show/1357725">B1 Maze</a> par “brynda1231” sous la licence <a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution Share Alike 3.0</a>. La licence <a href="https://www.gnu.org/licenses/quick-guide-gplv3.html">GPLV3</a> n'étant pas <a href="https://creativecommons.org/share-your-work/licensing-considerations/compatible-licenses">indiquée comme compatible</a> avec la <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA 3.0</a>, cette image, et elle seule, est placée sous la licence <a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution Share Alike 3.0</a>.

  <h2>Comment contacter l'auteur de ce site ?</h2>

  Vous pouvez contacter l'auteur à l'adresse suivante, <code>Romain.Casati_at_no_spam_ac-orleans-tours.fr</code>. Remplacer <code>_at_no_spam_</code> par <code>@</code>.