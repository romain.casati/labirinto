from labirinto import Maze, east, west, south, north
from tempfile import NamedTemporaryFile as TmpFile

import unittest


class TestInitMaze(unittest.TestCase):

    def test_init(self):
        for w, h in ((1, 1), (1, 2), (2, 1), (2, 3), (3, 2), (15, 16)):
            m = Maze(size=(w, h))

            self.assertEqual( m.width(), w )
            self.assertEqual( m.height(), h )

            self.assertTrue( m.opened(0, h - 1, west) )
            self.assertTrue( m.opened(w - 1, 0, east) )

            self.assertEqual( m.start(), (0, h - 1) )
            self.assertEqual( m.end(), (w - 1, 0) )

            for x in range(w):
                self.assertTrue( m.closed( x, 0, south ) )
                self.assertTrue( m.closed( x, h - 1, north ) )

            for y in range(h):
                if y < h - 1:
                    self.assertTrue( m.closed( 0, y, west ) )
                if y > 0:
                    self.assertTrue( m.closed( w - 1, y, east ) )

        m = Maze('labys/16x9.lab')
        self.assertEqual( m.width(), 16 )
        self.assertEqual( m.height(), 9 )

        
    def test_save(self):
        filename = 'labys/16x9.lab'
        m = Maze(filename)
        mfile = TmpFile(delete=False)
        m.save(mfile.name)

        with open(filename, 'rb') as f:
            self.assertEqual( mfile.read(), f.read())


if __name__ == '__main__':
    unittest.main()
