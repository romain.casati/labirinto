from labirinto import Maze
from tempfile import NamedTemporaryFile as TmpFile

import unittest


class TestSVGMaze(unittest.TestCase):

    def test(self):
        filename = 'labys/16x9.lab'
        m = Maze(filename)
        m.svg()

        mfile = TmpFile(delete=False)
        m.save_svg(mfile.name)
        with open(filename[:-3] + 'svg', 'rb') as f:
            s1, s2 = mfile.read(), f.read()
            s1 = s1.decode('utf-8').replace('\n', '')
            s2 = s2.decode('utf-8').replace('\n', '')
            self.assertEqual( s1, s2 )

        m.solve()
        m.svg()

if __name__ == '__main__':
    unittest.main()
