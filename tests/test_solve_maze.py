from labirinto import Maze, north, south, east, west

import unittest


class TestSolveMaze(unittest.TestCase):

    def test(self):
        filename = 'labys/16x9.lab'
        m = Maze(filename)
        paths, methods = [], ('left', 'right')
        for method in methods:
            m.solve(method=method, flush=True)
            path = m.paths(0)
            paths.append(path[:])
            path = path[1:-1]
            
            self.assertEqual( path[0], m.start() )
            self.assertEqual( path[-1], m.end() )

            directions = {(1, 0): east,
                          (-1, 0): west,
                          (0, 1): north,
                          (0, -1): south}
            
            for p1, p2 in zip(path[:-1], path[1:]):
                d = (p2[0] - p1[0], p2[1] - p1[1])
                self.assertIn( d, directions.keys() )
                self.assertTrue( m.opened( *p1, directions[d] ) )
        
        m.flush_paths()
        for method in methods:
            m.solve(method)
        self.assertEqual( m.paths(), paths )


if __name__ == '__main__':
    unittest.main()
