#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Romain Casati'
SITENAME = 'Labirinto'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

THEME = 'theme'

DIRECT_TEMPLATES = ('index', 'niveau_1', 'niveau_2', 'niveau_3', 'niveau_4', 'niveau_5', 'about')

STATIC_PATHS = [
    '.htaccess',
]

EXTRA_PATH_METADATA = {
    '.htaccess': {'path': '.htaccess'},
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = []

# Social widget
SOCIAL = []

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
