from labirinto.maze import Maze, direction2increment
from labirinto.maze import E as east
from labirinto.maze import W as west
from labirinto.maze import N as north
from labirinto.maze import S as south
from labirinto.default_mazes import mazes as maze_sizes
from labirinto.svg import SVG


default_element = document.getElementById("maze_container")

instructions = ["avancer", "aller_a_droite", "faire_demi_tour", "aller_a_gauche"]


class Niveau(object):
    def __init__(self, default="8x5"):
        self.maze = Maze(default=default)
        self.svg_path = None
        self.user_path = None
        self.user_finished = False

        self.interactive_draw = True

        self.message_finished = "Bravo ! Vous venez de débloquer le niveau suivant !"

        self.refresh_maze()

    def init_user_path(self):
        s = self.maze.start()
        self.user_path = [(s[0] - 0.25, s[1]), s]
        self.user_finished = False

    def redraw_path(self):
        self.svg_path.setAttribute("points", self.user_path)

    def refresh_maze(self):
        if self.maze is None:
            return
        svg_elem = self.maze.svg()
        child = default_element.firstChild
        if child is not None:
            default_element.removeChild(child)
        default_element.appendChild(svg_elem)

        paths_container = SVG()
        paths_container._node = svg_elem.getElementById("paths_container")
        self.svg_path = repr(
            SVG(
                parent=paths_container,
                tag="polyline",
                id="path",
                fill="none",
                stroke="rgb(66, 165, 245)",
                opacity="0.65",
                marker_end="url(#arrow)",
                points=[],
            )
        )

        self.init_user_path()
        self.redraw_path()

    def moveable(self, direction):
        if self.maze is None or self.user_finished:
            return False

        # __pragma__( 'opov' )
        pos = self.user_path[-1]
        # __pragma__( 'noopov' )
        if self.maze.opened(pos[0], pos[1], direction):
            new_pos = (
                pos[0] + direction2increment[direction][0],
                pos[1] + direction2increment[direction][1],
            )
            return (
                0 <= new_pos[0] < self.maze.width()
                and 0 <= new_pos[1] < self.maze.height()
            )
        return False

    def move(self, direction):
        if self.maze is None or self.user_finished:
            return False

        # __pragma__( 'opov' )
        pos = self.user_path[-1]
        moved = False
        if self.maze.opened(pos[0], pos[1], direction):
            new_pos = (
                pos[0] + direction2increment[direction][0],
                pos[1] + direction2increment[direction][1],
            )

            if new_pos == self.user_path[-2]:
                self.user_path.pop()
                moved = True
            elif (
                0 <= new_pos[0] < self.maze.width()
                and 0 <= new_pos[1] < self.maze.height()
            ):
                self.user_path.append(new_pos)
                moved = True

        if self.user_path[-1] == self.maze.end():
            self.user_finished = True
            self.user_path.append((self.user_path[-1][0] + 0.25, self.user_path[-1][1]))
        # __pragma__( 'noopov' )

        if self.interactive_draw:
            self.redraw_path()

        if self.user_finished:
            if not self.interactive_draw:
                self.redraw_path()
            event = document.createEvent("Event")
            event.initEvent("uplevel", True, True)
            document.dispatchEvent(event)
            window.alert(self.message_finished)

        return moved


class Niveau1(Niveau):
    def __init__(self):
        Niveau.__init__(self)  # can't use super in transcrypt backward compat

        self.btns = [
            document.getElementById("btn_{}".format(x))
            for x in ("left", "up", "right", "down")
        ]

        window.addEventListener("keydown", self.key_down)
        window.addEventListener("keyup", self.key_up)

        for btn, direction in zip(self.btns, (west, north, east, south)):
            # transcrypt hack, see https://github.com/QQuick/Transcrypt/issues/36
            btn.onclick = (lambda x: lambda: self.move(x))(direction)

    def button(self, keycode):
        index = keycode - 37
        return self.btns[index] if 0 <= index < len(self.btns) else None

    def key_up(self, event):
        btn = self.button(event.keyCode)
        if btn is not None:
            btn.click()
            btn.classList.remove("active")

    def key_down(self, event):
        btn = self.button(event.keyCode)
        if btn is not None:
            btn.classList.add("active")
            event.preventDefault()


def timer(length):
    def timer_elapse(resolve):
        setTimeout(lambda: resolve(), length * 1000)

    return __new__(Promise(timer_elapse, lambda: console.log("exception in timer")))


class Niveau2(Niveau):
    def __init__(self, default="8x5"):
        Niveau.__init__(self, default)  # can't use super in transcrypt backward compat
        self.editor = ace.edit("editor")

        self.reinit_btn = document.getElementById("reinit")
        self.launch_btn = document.getElementById("launch")
        self.stop_btn = document.getElementById("stop")

        self.reinit_btn.addEventListener("click", self.reinit)
        self.launch_btn.addEventListener("click", self.launch)
        self.btns = [self.reinit_btn, self.launch_btn]
        self.stop_btn.addEventListener("click", self.stop)

        self.running = False

        self.direction = east

    def reinit(self):
        self.direction = east
        self.init_user_path()
        self.redraw_path()

    def disable_btns(self, value=True):
        for b in self.btns:
            b.disabled = value
        self.stop_btn.disabled = not value

    async def launch(self):
        self.reinit()
        self.disable_btns()

        self.running = True

        code = self.editor.getValue()
        code = code.replace(" ", "").split("\n")

        for i, c in enumerate(code):
            if not self.running:
                break
            if c == "":
                continue
            await timer(0.5)
            self.editor.clearSelection()
            self.editor.selection.moveCursorToPosition({"row": i, "column": 0})
            dir = self.direction
            new_dir = dict(zip(instructions, [(dir + k) % 4 for k in range(4)]))
            if c not in new_dir:
                self.editor.selection.selectLine()
                break
            new_dir = new_dir[c]
            if self.move(new_dir):
                self.direction = new_dir

        self.disable_btns(False)

    def stop(self):
        self.running = False


class Niveau3(Niveau2):
    def __init__(self, default="32x18"):
        Niveau2.__init__(self, default)  # can't use super in transcrypt backward compat

    def launch(self):
        code = self.editor.getValue()
        if "niveau_suivant" in code:
            self.user_path.append(self.maze.end())
            self.move(east)
        else:
            Niveau2.launch(self)  # can't use super in transcrypt backward compat


class Niveau4(Niveau2):
    def __init__(self, default="32x18"):
        Niveau2.__init__(self, default)  # can't use super in transcrypt backward compat
        self.delay = 0.05
        self.maxiter = 1000

    def build_user_func(self):
        code = self.editor.getValue()
        try:
            code = escodegen.generate(filbert.parse(code))
        except:
            return None

        tests = ["peut_" + i for i in instructions]
        for i in instructions:
            code = code.replace(" " + i, " return " + i)
        # __pragma__( 'opov' )
        code = "(function (" + ", ".join(tests + instructions) + ") {" + code + "})"
        # __pragma__( 'noopov' )
        try:
            func = window.eval(code)
        except:
            return None

        return func

    def next_direction(self):
        dir = self.direction
        dirs = [(dir + k) % 4 for k in range(4)]
        moveables = [self.moveable(d) for d in dirs]
        try:
            return self.user_func(*moveables, *dirs)
        except:
            return None

    async def launch(self):
        self.reinit()
        self.disable_btns()

        self.user_func = self.build_user_func()
        self.running = self.user_func != None

        niter = 0
        while self.running and not (
            self.user_finished or len(self.user_path) > self.maxiter
        ):
            if self.delay > 0:
                await timer(self.delay)
            elif niter % 10**3 == 0:
                await timer(0.05)

            new_dir = self.next_direction()
            if new_dir == None:
                break
            if self.move(new_dir):
                self.direction = new_dir
            else:
                break
            niter += 1

        self.disable_btns(False)


class Niveau5(Niveau4):
    def __init__(self, default="128x72"):
        Niveau4.__init__(self, default)  # can't use super in transcrypt backward compat
        self.delay = 0
        self.maxiter = 10**5
        self.interactive_draw = False
        self.message_finished = "Bravo ! Vous avez fini l'activité !"

        self.select = document.getElementById("selectsize")
        for s in maze_sizes.keys():
            o = document.createElement("option")
            o.text = s
            self.select.add(o)
        self.select.value = default

        generate_btn = document.getElementById("generate")

        self.btns.append(self.select)
        self.btns.append(generate_btn)

        self.select.addEventListener("change", self.change_maze)
        generate_btn.addEventListener("click", self.generate_maze)

    def change_maze(self):
        self.maze = Maze(default=self.select.value)
        self.refresh_maze()

    def generate_maze(self):
        size = [int(x) for x in self.select.value.split("x")]
        self.maze = Maze(size=size)
        self.refresh_maze()
