\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{activite}[2019/02/26 v1.0 custom article class]

% custom article class
\LoadClass[a4paper,11pt]{article}

\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[francais]{babel}

% geometry
\usepackage[%
head=0pt,%
foot=0pt,%
left=1.5cm,%
right=1.5cm,%
top=2cm,%
bottom=1.5cm]{geometry}

\usepackage{lmodern}

\usepackage[inline]{enumitem}
\setenumerate[1]{label=\arabic*)}
\setenumerate[2]{label=\alph*)}
\setenumerate[3]{label=\roman*)}
% bullet in itemize
\frenchbsetup{StandardLists=true}
\setlist[enumerate,itemize]{topsep=0pt,itemsep=0pt}

\usepackage{tikz}
\tikzset{
  every picture/.style=thick, % default to thick
  every picture/.prefix style={
    execute at begin picture=\shorthandoff{;}
  } % avoid conflict with french babel
}

% colors
\definecolor{colortheme1}{RGB}{66, 165, 245}
\definecolor{colortheme2}{RGB}{186, 33, 33}
\definecolor{colortheme3}{RGB}{102, 187, 106}
%\definecolor{colortheme4}{rgb}{0.627, 0.231, 0.651}
%\definecolor{colortheme5}{rgb}{0, 0.643, 0.996}
\extractcolorspec{colortheme1}{\colorthemespec}
\expandafter\convertcolorspec\colorthemespec{rgb}\colorthemespec
\definecolor{colortheme}{rgb}{\colorthemespec}

\makeatletter
\def\maketitle{%
% 
\sloppy
% 
{\center\Large\bfseries \@title\\}%
\vspace{0\baselineskip}%
}
\makeatother

\usepackage[hidelinks]{hyperref}
% 
% custom page layout
\usepackage{fancyhdr,lastpage}
\setlength{\headheight}{.5\baselineskip}
\pagestyle{fancy}
\renewcommand\headrulewidth{.8pt}
\fancyhead[L]{Numérique et Sciences Informatiques}
\fancyhead[R]{Lycée Henri Brisson}
\fancyfoot[C]{}
\fancyfoot[R]{Page \thepage\ sur \pageref{LastPage}}

% indented minipage
\newenvironment{minipageindent}[1]{%
  \edef\myindent{\the\parindent}%
  \begin{minipage}{#1}%
    \setlength{\parindent}{\myindent}}{\end{minipage}}

\usepackage{minted}
\usepackage{tcolorbox}
\usepackage{fancyvrb}

\newminted[pythoncmdcode]{pycon}{mathescape}
\newminted[pythoncode]{python}{mathescape}
\newminted[jupytercode]{python}{mathescape}
\BeforeBeginEnvironment{jupytercode}{\begin{tcolorbox}[boxrule=0.65pt,
    colback=black!2]}%
  \AfterEndEnvironment{jupytercode}{\end{tcolorbox}}%

\newenvironment{jupyterres}%
{\endgraf\setlength\partopsep{-\topsep}\Verbatim}%
{\endVerbatim\vspace{.5\baselineskip}}

\newcommand{\pythoninline}{\mintinline{python}}

\endinput