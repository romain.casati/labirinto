if peut_aller_a_gauche:
    aller_a_gauche
else:
    if peut_avancer:
        ...
    else:
        if ...:
            ...
        else:
            ...
