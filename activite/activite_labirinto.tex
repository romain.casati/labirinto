\documentclass{activite}

\usepackage[francais]{babel}

\usepackage{qrcode}

\usepackage{subcaption}

\usetikzlibrary{arrows.meta}

\def\urlsite{http://labirinto.infobrisson.fr}

\title{Sortir d'un labyrinthe\\ \large avec un algorithme}

\begin{document}
%
\maketitle
%
\vspace{-0.5\baselineskip}
%
\section*{Introduction}
%
\noindent
\begin{minipageindent}{.9\textwidth}
  % 
  Le but de cette activité est de comprendre comment on peut
  programmer un algorithme pour sortir d'un labyrinthe. Pour cela,
  nous utiliserons le site «~Labirinto~», associé à cette
  activité. Vous le trouverez à l'adresse \url{\urlsite} ou en
  flashant le code ci contre.
  %
\end{minipageindent}
% 
\hfill
% 
\begin{tabular}{@{}c@{}}
  % 
  \qrcode[height=.08\textwidth]{\urlsite}
  % 
\end{tabular}

\paragraph{Quel navigateur utiliser ?} À l'heure actuelle, ce site
n'est \emph{pas} compatible avec Internet Explorer qui ne respecte pas
certains standards \texttt{JavaScript}\footnote{Par exemple, comme il
  est indiqué
  \href{https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/async_function\#Compatibilit\%C3\%A9_des_navigateurs}{ici},
  Internet Explorer ne supporte pas les fonctions asynchrones.}. Pour
vous connecter au site de l'activité, utilisez donc un autre
navigateur (comme Firefox ou Chrome) dans une version récente.

\paragraph{Déroulement de l'activité.} Cette activité se décompose en
5 niveaux qui vous amènerons graduellement à la résolution automatique
de labyrinthes aussi complexes que celui ci-dessous (voire même plus
complexes, si votre machine le supporte). Le premier niveau consiste à
résoudre de petits labyrinthes à la main et le dernier niveau vise à
utiliser l'algorithme développé aux niveaux 2, 3, et 4.
% 
\begin{figure}[htb!]
  %
  \hfil
  \includegraphics[width=.8\textwidth]{hard_maze}
  %
  \caption{Résolution automatique d'un labyrinthe de taille
    $200 \times 50$ à l'aide de l'algorithme découvert dans cette
    activité.}
  % 
\end{figure}
%
\vspace{-1\baselineskip}
%
\paragraph{Vocabulaire utilisé.} Dans cette activité, un
\emph{labyrinthe} est un tableau rectangulaire contenant des
\emph{cellules} séparées ou non par des \emph{murs}. Le \emph{départ}
du labyrinthe se situe toujours \emph{en haut à gauche} et l'arrivée,
\emph{en bas à droite}. La situation dans laquelle vous vous trouverez
au début de la résolution d'un labyrinthe est celle de la figure
ci-dessous. Les labyrinthes que nous utilisons sont dit
\emph{parfaits}, c'est-à-dire que chaque cellule est reliée à toutes
les autres et, ce, de manière unique. En particulier, il n'existe pas
d'endroit inaccessible dans le labyrinthe et on ne peut pas revenir
sur une cellule déjà visitée sans rebrousser chemin.
% 

\begin{figure}[htb!]
  % 
  \hfil
  \includegraphics[width=.8\textwidth]{simple_maze}
  %
  \caption{Situation au départ de la résolution d'un labyrinthe de
    taille $10 \times 4$.}
  % 
\end{figure}
%
\paragraph{C'est parti !} Lorsque vous avez fini de lire ceci, vous
pouvez commencer le premier niveau de l'activité.
% 
\section*{Niveau 1}
%
Essayez d'atteindre la sortie du labyrinthe à l'aide du clavier ou des
flèches directionnelles.
% 
\section*{Niveau 2}
% 
Pour décrire un algorithme à un ordinateur, on utilise plutôt une
suite d'instructions (sous forme de texte) que des flèches.

Le labyrinthe est le même que celui du niveau précédent. Décrivez à
l'ordinateur comment sortir du labyrinthe à l'aide des quatre
instructions suivantes :
% 
\begin{itemize}
  % 
\item \texttt{avancer}
  %
\item \texttt{faire\_demi\_tour}
  %
\item \texttt{aller\_a\_gauche}
  % 
\item \texttt{aller\_a\_droite}
  % 
\end{itemize}

Vous devez saisir une instruction par ligne et bien tenir compte du
sens de la flèche. Aller à gauche n'est pas à comprendre «~par rapport
à l'écran~» mais «~par rapport à la flèche~» ! Les déplacements se
font de cellule en cellule.
% 
\section*{Niveau 3}
%
Que pensez-vous de la résolution d'un tel labyrinthe avec la méthode
du niveau précédent ?

\paragraph{} En effet, ce serait beaucoup trop long à écrire ! Une des
principales qualités requises en informatique est la fainéantise : les
informaticiens détestent faire des choses rébarbatives et préfèrent
les automatiser.

\paragraph{} Pour découvrir quel est l'algorithme simple qui permet de
sortir de ce labyrinthe et le mettre en œuvre, rendez-vous au niveau
suivant. Pour cela, taper \texttt{niveau\_suivant} dans la zone à
droite du labyrinthe puis lancez le programme.
% 
\section*{Niveau 4}
%
\paragraph{Algorithme.} Une méthode très simple pour sortir d'un
labyrinthe parfait est celle de «~la main contre le mur~». Elle
consiste à utiliser sa main gauche (ou sa main droite), de la poser
contre le mur et d'avancer sans jamais décoller sa main.

Plus précisément, si l'on a choisi la main gauche, s'il n'y a pas de
mur à gauche, on est obligé d'y aller, sinon on va décoller sa main du
mur. Sinon, s'il n'y a pas de mur en face, on va tout droit et ainsi
de suite. La figure ci-dessous résume les quatre situations possibles,
pour la méthode de «~la main gauche~» et celle de «~la main droite~».

\begin{figure}[htb!]
  %
  \begin{subfigure}[b]{\textwidth}
    %
    \hfil
    \begin{tikzpicture}[line join=round, line cap=round]
      %
      \clip (-0.05, -1.5) rectangle (2.5, 0.5);
      % 
      \begin{scope}[black, double=black, double distance = 0.015cm]
        %
        \draw[double]
        (0, 0) -- (1, 0)
        (0, -1) -- (1, -1);
        %
      \end{scope}
      %
      \begin{scope}[colortheme, double=colortheme, double distance =
        0.2cm, shift={(0.5, -0.5)}]
        % 
        \draw[double, -{Triangle[length=0.3cm,width=0.6cm]}]
        (0, 0) -- (1, 0) -- (1, 1);
        %
      \end{scope}
      % 
    \end{tikzpicture}
    %
    \hfil
    %
    \begin{tikzpicture}[line join=round, line cap=round]
      %
      \clip (-0.05, -1.5) rectangle (2.5, 0.5);
      % 
      \begin{scope}[black, double=black, double distance = 0.015cm]
        %
        \draw[double]
        (0, 0) -- (2, 0)
        (0, -1) -- (1, -1);
        %
      \end{scope}
      %
      \begin{scope}[colortheme, double=colortheme, double distance =
        0.2cm, shift={(0.5, -0.5)}]
        % 
        \draw[double, -{Triangle[length=0.3cm,width=0.6cm]}]
        (0, 0) -- (2, 0);
        %
      \end{scope}
      % 
    \end{tikzpicture}
    %
    \hfil
    %
    \begin{tikzpicture}[line join=round, line cap=round]
      %
      \clip (-0.05, -1.5) rectangle (2.5, 0.5);
      % 
      \begin{scope}[black, double=black, double distance = 0.015cm]
        %
        \draw[double]
        (0, 0) -- (2, 0) -- (2, -1)
        (0, -1) -- (1, -1)
        ;
        %
      \end{scope}
      %
      \begin{scope}[colortheme, double=colortheme, double distance =
        0.2cm, shift={(0.5, -0.5)}]
        % 
        \draw[double, -{Triangle[length=0.3cm,width=0.6cm]}]
        (0, 0) -- (1, 0) -- (1, -1);
        %
      \end{scope}
      % 
    \end{tikzpicture}
    %
    \hfil
    %
    \begin{tikzpicture}[line join=round, line cap=round]
      %
      \clip (-0.05, -1.5) rectangle (2.5, 0.5);
      % 
      \begin{scope}[black, double=black, double distance = 0.015cm]
        %
        \draw[double]
        (0, 0) -- (2, 0) -- (2, -1) -- (0, -1);
        %
      \end{scope}
      %
      \begin{scope}[colortheme, double=colortheme, double distance =
        0.2cm, shift={(0.5, -0.5)}]
        % 
        \draw[double, -{Triangle[length=0.3cm,width=0.6cm]}]
        (0, 0) -- (1, 0) -- (0, 0);
        %
      \end{scope}
      % 
    \end{tikzpicture}
    % 
    \caption{Méthode de la main gauche.}
    % 
    \label{fig:maingauche}
    % 
  \end{subfigure}
  
  \begin{subfigure}[b]{\textwidth}
    % 
    \hfil
    \begin{tikzpicture}[line join=round, line cap=round]
      %
      \clip (-0.05, -1.5) rectangle (2.5, 0.5);
      % 
      \begin{scope}[black, double=black, double distance = 0.015cm]
        %
        \draw[double]
        (0, 0) -- (1, 0)
        (0, -1) -- (1, -1);
        %
      \end{scope}
      %
      \begin{scope}[colortheme, double=colortheme, double distance =
        0.2cm, shift={(0.5, -0.5)}]
        % 
        \draw[double, -{Triangle[length=0.3cm,width=0.6cm]}]
        (0, 0) -- (1, 0) -- (1, -1);
        %
      \end{scope}
      % 
    \end{tikzpicture}
    %
    \hfil
    %
    \begin{tikzpicture}[line join=round, line cap=round]
      %
      \clip (-0.05, -1.5) rectangle (2.5, 0.5);
      % 
      \begin{scope}[black, double=black, double distance = 0.015cm]
        %
        \draw[double]
        (0, 0) -- (1, 0)
        (0, -1) -- (2, -1);
        %
      \end{scope}
      %
      \begin{scope}[colortheme, double=colortheme, double distance =
        0.2cm, shift={(0.5, -0.5)}]
        % 
        \draw[double, -{Triangle[length=0.3cm,width=0.6cm]}]
        (0, 0) -- (2, 0);
        %
      \end{scope}
      % 
    \end{tikzpicture}
    %
    \hfil
    %
    \begin{tikzpicture}[line join=round, line cap=round]
      %
      \clip (-0.05, -1.5) rectangle (2.5, 0.5);
      % 
      \begin{scope}[black, double=black, double distance = 0.015cm]
        %
        \draw[double]
        (0, 0) -- (1, 0)
        (0, -1) -- (2, -1) -- (2, 0)
        ;
        %
      \end{scope}
      %
      \begin{scope}[colortheme, double=colortheme, double distance =
        0.2cm, shift={(0.5, -0.5)}]
        % 
        \draw[double, -{Triangle[length=0.3cm,width=0.6cm]}]
        (0, 0) -- (1, 0) -- (1, 1);
        %
      \end{scope}
      % 
    \end{tikzpicture}
    %
    \hfil
    %
    \begin{tikzpicture}[line join=round, line cap=round]
      %
      \clip (-0.05, -1.5) rectangle (2.5, 0.5);
      % 
      \begin{scope}[black, double=black, double distance = 0.015cm]
        %
        \draw[double]
        (0, 0) -- (2, 0) -- (2, -1) -- (0, -1);
        %
      \end{scope}
      %
      \begin{scope}[colortheme, double=colortheme, double distance =
        0.2cm, shift={(0.5, -0.5)}]
        % 
        \draw[double, -{Triangle[length=0.3cm,width=0.6cm]}]
        (0, 0) -- (1, 0) -- (0, 0);
        %
      \end{scope}
      % 
    \end{tikzpicture}
    % 
    \caption{Méthode de la main droite.}
    % 
    \label{fig:maindroite}
    % 
  \end{subfigure}
  \caption{Les quatre situations possibles, pour la méthode de «~la
    main gauche~» (\subref{fig:maingauche}) et celle de «~la main droite~»
    (\subref{fig:maindroite}).}
  % 
\end{figure}
%
\paragraph{À faire.} Votre travail consiste à compléter le programme
suivant pour implémenter la méthode de «~la main gauche~» :
%
\inputminted[autogobble]{python}{squelette.py}
%
C'est l'ordinateur qui s'occupe de répéter ces actions jusqu'à ce que
l'on trouve la sortie du labyrinthe. On rappels les quatre
instructions utiles :
% 
\begin{itemize}
  % 
\item \texttt{avancer}
  %
\item \texttt{faire\_demi\_tour}
  %
\item \texttt{aller\_a\_gauche}
  % 
\item \texttt{aller\_a\_droite}
  % 
\end{itemize}

\paragraph{Bonus.} Écrire le programme de la méthode de «~la main
droite~».
% 
\section*{Niveau 5}
%
Reprenez le programme développé au niveau précédent et testez-le sur
les différents labyrinthes proposés.
% 
\end{document}
% 
%%% Local Variables:
%%% LaTeX-command: "latex -shell-escape"
%%% End:
