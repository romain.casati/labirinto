import xml.etree.ElementTree as et

from .svg_nodes import nodes

def _manage(dictio):
    return {k.replace('_', '-'): str(v) for (k, v) in dictio.items()}


class SVG(object):
    """
    This class represents a SVG document based on etree.
    Supported nodes are:
        g, line, polyline, defs, marker and path

    To get a string representation of the document, use the str method.
    """
    def __init__(self, parent=None, tag='', **kwargs):
        kwargs = _manage(kwargs)
        if parent is None:
            self._node = et.Element('svg', attrib=kwargs)
        else:
            self._node = et.SubElement(parent._node, tag, attrib=kwargs)
        
        for f in nodes:
            def func(_s=self, _f=f, **_kwargs):
                return SVG(_s, _f, **_kwargs)
            setattr(self, f, func)

    def __repr__(self):
        return self._node

    def __str__(self):
        return et.tostring(self._node).decode('utf-8')
