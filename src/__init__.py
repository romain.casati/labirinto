from .maze import Maze
from .maze import E as east
from .maze import W as west
from .maze import S as south
from .maze import N as north
from .maze import DIRECTIONS as directions
