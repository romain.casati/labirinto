svg_type = None
SVG = None

# __pragma__( 'skip' )

try:
    from .svg_brython import SVG
    svg_type = 'brython'
except ImportError:
    from .svg_etree import SVG
    svg_type = 'etree'

# __pragma__( 'noskip' )

if svg_type is None:
    from labirinto.svg_transcrypt import SVG as _SVG
    SVG = _SVG
    svg_type = 'transcrypt'
