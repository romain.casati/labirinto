from browser import html, svg

from .svg_nodes import nodes


def _manage(dictio):
    return {k.replace('_', '-'): v for (k, v) in dictio.items()}

class SVG(object):
    """
    This class is a wraper around the Brython's SVG representation.
    Supported nodes are:
        g, line, polyline, defs, marker and path
    """
    def __init__(self, parent=None, tag_func=html.SVG, **kwargs):
        kwargs = _manage(kwargs)
        if parent is None:
            self._node = html.SVG(**kwargs)
        else:
            self._node = tag_func(**kwargs)
            parent._node <= self._node

        for f in nodes:
            def func(_s=self, _f=f, **_kwargs):
                return SVG(self, getattr(svg, _f), **_kwargs)
            setattr(self, f, func)

    def __repr__(self):
        return self._node
    
    def __str__(self):
        dummy = html.SVG()
        dummy <= self._node
        return dummy.html
