import random


# south, north, east, west
N, E, S, W = 0, 1, 2, 3
DIRECTIONS = (N, E, S, W)
direction2increment = [(0, 1), (1, 0), (0, -1), (-1, 0)]


def _bool_array_to_str(array):
    return ' '.join(''.join(str(int(b)) for b in l) for l in array)


def _str_to_bool_array(s):
    return [[bool(int(c)) for c in l] for l in s.split(' ')]


class Maze(object):
    def __init__(self, filename=None, size=None, default=None):
        if filename is not None or default is not None:
            if filename is not None:
                with open(filename, 'r') as f:
                    data = f.read()
            elif default is not None:
                from labirinto.default_mazes import mazes
                data = mazes[default]

            vwalls, hwalls = data.split('\n')
            self._vertical_walls = _str_to_bool_array(vwalls)
            self._horizontal_walls = _str_to_bool_array(hwalls)
            self._size = (len(self._horizontal_walls),
                          len(self._vertical_walls[0]))
        elif size is not None:
            self._size = size
            self._generate_random()
        else:
            raise ValueError('One of <filename> or <size> should be set.')

        self._paths = []

        # start is always upper left and end is always lower right
        self._start = (0, self.height() - 1)
        self._end = (self.width() - 1, 0)

        self.break_wall(*self._start, W)
        self.break_wall(*self._end, E)

    def width(self):
        return self._size[0]

    def height(self):
        return self._size[1]

    def start(self):
        return self._start

    def end(self):
        return self._end

    def save(self, filename):
        with open(filename, 'w') as f:
            f.write(_bool_array_to_str(self._vertical_walls))
            f.write('\n')
            f.write(_bool_array_to_str(self._horizontal_walls))

    def __getitem__(self, key):
        x, y, direction = key
        if direction in (W, E):
            return self._vertical_walls[x + 1 - direction // 2][y]
        else:
            return self._horizontal_walls[x][y + 1 - direction // 2]

    def __setitem__(self, key, value):
        x, y, direction = key
        if direction in (W, E):
            self._vertical_walls[x + 1 - direction // 2][y] = value
        else:
            self._horizontal_walls[x][y + 1 - direction // 2] = value

    def closed(self, x, y, direction):
        return self[x, y, direction]

    def opened(self, x, y, direction):
        return not self.closed(x, y, direction)

    def neighbours(self, x, y):
        n = []
        for d in DIRECTIONS:
            nx, ny = (x + direction2increment[d][0],
                      y + direction2increment[d][1])
            if (0 <= nx < self._size[0]) and (0 <= ny < self._size[1]):
                n.append(((nx, ny), d))
        return n

    def accessible_neighbours(self, x, y):
        return [(cell, d) for (cell, d) in self.neighbours(x, y)
                if self.opened(x, y, d)]

    def break_wall(self, x, y, direction):
        self[x, y, direction] = False

    def ncells(self):
        return self.width() * self.height()

    def random_cell(self):
        return (random.randint(0, self.width() - 1),
                random.randint(0, self.height() - 1))

    def lines(self):
        vlines = [((x, y), (x, y + 1))
                  for x in range(self._size[0] + 1)
                  for y in range(self._size[1])
                  if self._vertical_walls[x][y]]
        hlines = [((x, y), (x + 1, y))
                  for x in range(self._size[0])
                  for y in range(self._size[1] + 1)
                  if self._horizontal_walls[x][y]]
        # __pragma__ ('opov')
        return vlines + hlines
        # __pragma__ ('noopov')

    def _generate_random(self):
        class CellArray(object):
            def __init__(self, shape):
                # __pragma__( 'opov' )
                self._array = [[False] * shape[1] for n in range(shape[0])]
                # __pragma__( 'noopov' )

            def __getitem__(self, cell):
                return self._array[cell[0]][cell[1]]

            def __setitem__(self, cell, value):
                self._array[cell[0]][cell[1]] = value
        # __pragma__ ('opov')
        self._vertical_walls = [[True] * self._size[1]
                                for x in range(self._size[0] + 1)]
        self._horizontal_walls = [[True] * (self._size[1] + 1)
                                  for x in range(self._size[0])]
        # __pragma__ ('noopov')
        cell = self.random_cell()
        visited = CellArray(self._size)
        visited[cell] = True
        queue = [cell]
        nvisited = 1

        while nvisited < self.ncells():
            # all non visited neighbours
            neighbours = [n for n in self.neighbours(*cell)
                          if not visited[n[0]]]
            # __pragma__( 'tconv' )
            if not neighbours:
                cell = queue.pop()
                continue
            # __pragma__( 'notconv' )

            # random non visited neighbor
            next_cell, direction = random.choice(neighbours)
            # beaking the wall and continuing with new cell
            self.break_wall(*cell, direction)
            cell = next_cell
            visited[cell] = True
            queue.append(cell)
            nvisited += 1

    def flush_paths(self):
        self._paths = []

    def paths(self, index=None):
        if index is None:
            return self._paths
        else:
            return self._paths[index]

    def solve_bfs(self):
        class Node(object):
            def __init__(self, parent=None, data=None):
                self.parent = parent
                self.data = data
        
        queue = [Node(parent=None, data=self.start())]
        while len(queue) > 0:
            node = queue.pop()
            # __pragma__( 'opov' )
            if node.data == self.end():
                res = [node.data]
                while node.parent is not None:
                    node = node.parent
                    res.insert(0, node.data)
                return res
            else:
                queue = ([Node(parent=node, data=n)
                          for (n, d) in self.accessible_neighbours(*node.data)
                          if node.parent is None or node.parent.data != n]
                         + queue)
            # __pragma__( 'noopov' )
        raise RuntimeError('Escape not found by BFS algorithm.')

    def solve_hand(self, hand='left'):
        if hand == 'left':
            lookup = lambda d: [(d + 3 + i) % 4 for i in range(4)]
        elif hand == 'right':
            lookup = lambda d: [(d + 5 - i) % 4 for i in range(4)]
        else:
            raise ValueError('<hand> should be "left" or "right".')

        cell, direction = self._start, E
        path = [cell]
        # __pragma__( 'opov' )
        while cell != self._end:
            for d in lookup(direction):
                if self.opened(*cell, d):
                    direction = d
                    break
            inc = direction2increment[direction]
            cell = (cell[0] + inc[0], cell[1] + inc[1])
            path.append(cell)
        return path
        # __pragma__( 'noopov' )

    def solve(self, method='bfs', flush=False):
        if flush:
            self.flush_paths()

        if method in ('left', 'right'):
            path = self.solve_hand(hand=method)
        elif method == 'dfs':
            path = self.solve_dfs()
        else:
            path = self.solve_bfs()
        # __pragma__( 'opov' )
        self._paths.append([(path[0][0] - 0.25, path[0][1])] + path
                           + [(path[-1][0] + 0.25, path[-1][1])])
        # __pragma__( 'noopov' )

    def svg(self, scale=400, wall_color='black',
            path_colors=('rgb(66, 165, 245)',
                         'rgb(186, 33, 33)',
                         'rgb(102, 187, 106)'),
            wall_lw=0.07, path_lw=0.25, id='svg_maze', result='auto'):

        from labirinto.svg import SVG, svg_type

        if result == 'auto':
            result = 'html' if svg_type == 'etree' else 'dom'

        scale = scale / max(self.width() / 2, self.height())
        svg = SVG(id=id,
                  preserveAspectRatio='xMinYMin meet',
                  viewBox='0 0 {} {}'.format(
                      (self.width() + wall_lw) * scale + 2,
                      (self.height() + wall_lw) * scale + 2))

        svg_all = svg.g(transform='matrix({}, 0, 0, {}, {}, {})'.format(
            scale, -scale, wall_lw * scale/2 + 1,
            wall_lw * scale/2 + 1 + self.height() * scale),
                            stroke_linecap='round')

        svg_walls = svg_all.g(id='walls_container',
                              stroke=wall_color,
                              stroke_width=wall_lw)

        svg_paths = svg_all.g(id='paths_container',
                              stroke_width=path_lw,
                              stroke_linejoin='round',
                              transform='translate(0.5, 0.5)')

        svg_defs = svg_paths.defs()
        svg_marker = svg_defs.marker(id='arrow', orient='auto',
                                     markerWidth=2, markerHeight=4,
                                     refX=0, refY=1)

        svh_arrow_path = svg_marker.path(d='M0,0 V2 L1,1 Z',
                                         fill=path_colors[0])  # hack since context-stroke is not working
#                                         fill='context-stroke')

        for p1, p2 in self.lines():
            svg_walls.line(x1=p1[0], y1=p1[1], x2=p2[0], y2=p2[1])

        # __pragma__('opov')
        for path, color in zip(self.paths(), path_colors * len(self.paths())):
            svg_paths.polyline(fill='none',
                               opacity=0.65,
                               stroke=color,
                               marker_end='url(#arrow)',
                               points=' '.join(['{},{}'.format(*p)
                                                for p in path]))
        # __pragma__('noopov')

        return str(svg) if result == 'html' else repr(svg)

    def save_svg(self, filename, **kwargs):
        with open(filename, 'w') as f:
            f.write(self.svg(**kwargs))

    def _repr_svg_(self):
        return self.svg()
