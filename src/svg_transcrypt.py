from labirinto.svg_nodes import nodes


def _manage(dictio):
    return {k.replace('_', '-'): str(v) for (k, v) in dictio.items()}


_svg_uri = 'http://www.w3.org/2000/svg'


class SVG(object):
    """
    This class represents a SVG document based on etree.
    Supported nodes are:
        g, line, polyline, defs, marker and path

    To get a string representation of the document, use the str method.
    """
    def __init__(self, parent=None, tag='', **kwargs):
        kwargs = _manage(kwargs)
        if parent is None:
            self._node = document.createElementNS(_svg_uri, 'svg')
            for k, v in kwargs.items():
                self._node.setAttribute(k, v)
        else:
            self._node = document.createElementNS(_svg_uri, tag)
            for k, v in kwargs.items():
                self._node.setAttribute(k, v)
            parent._node.appendChild(self._node)
        for f in nodes:
            # transcrypt hack, see https://github.com/QQuick/Transcrypt/issues/36
            setattr(self, f, (lambda x: lambda **_kwargs:
                              SVG(self, x, **_kwargs))(f))

    def __repr__(self):
        return self._node

    def __str__(self):
        dummy = document.createElement('dummy')
        dummy.appendChild(self._node)
        return dummy.innerHTML
